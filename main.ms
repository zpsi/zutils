//[Zutils Run on server start procedures.]
//Repository: https://gitlab.com/zpsi/zutils/tree/master/

bind(world_changed, null, array(to: 'minigames'), @event){
	sudo('/undisguise '.@event['player']);
}

bind(player_command, null, null, @event){
	if(to_lower(@event['prefix']) == '/sg' || to_lower(@event['prefix']) == '/survivalgames' || to_lower(@event['prefix']) == '/ma' || to_lower(@event['prefix']) == '/mobarena' || to_lower(@event['prefix']) == '/tntrun' || to_lower(@event['prefix']) == '/tntrun:tntrun' || to_lower(@event['prefix']) == '/tr'){
		@args = parse_args($);
		if(array_size(@args) > 1){
			if(to_lower(@args[1]) == 'join' || to_lower(@args[1]) == 'j' || to_lower(@args[1]) == 'spec' || to_lower(@args[1]) == 's'){
				if(array_size(@args) < 3){
					cancel();
					_error(4);
				}
				cancel();
				_wait(3, closure(){
					run(@event['command']);
				});
			}
		}
		die();
	}
	if(to_lower(@event['prefix']) == '/sudo'){
		@args = parse_args(@event['command']);
		if(is_alias(@args[2])){
			cancel();
			run('/sudo '.@args[1].' runalias '.@args[2]);
		}
		die();
	}
	if(to_lower(@event['prefix']) == '/tp' && !has_permission(player(), 'essentials.tp')){
		@args = parse_args(@event['command']);
		cancel();
		run('/tpa '.@args[1]);
	}
}

bind(player_interact, null, null, @event){
	if(@event['block'] != 0){
		for(@i = 1, @i <= get_value('zperm.regclick.id'), @i++){
			if(!has_value('zperm.regclick.pos.'.@i)){
				continue();
			}
			if(double(get_value('zperm.regclick.pos.'.@i)[0]) == double(@event['location'][0]) && double(get_value('zperm.regclick.pos.'.@i)[1]) == double(@event['location'][1]) && double(get_value('zperm.regclick.pos.'.@i)[2]) == double(@event['location'][2]) && get_value('zperm.regclick.pos.'.@i)[3] == @event['location'][3] && get_value('zperm.regclick.action.'.@i) == @event['action']){
				if(!has_value('zutils.regclick.buffer.'.to_lower(player()))){
					set_timeout(get_value('zperm.regclick.buffer.'.@i) * 1000, closure(){
						clear_value('zutils.regclick.buffer.'.to_lower(player()));
					})
					store_value('zutils.regclick.buffer.'.to_lower(player()), 1);
					if(split('', get_value('zperm.regclick.run.'.@i))[0] == '/'){
						run(get_value('zperm.regclick.run.'.@i));
						die();
					}
					else{
						_regclickrun(get_value('zperm.regclick.run.'.@i));
					}
				}
			}
		}
	}
}

bind(block_break, null, null, @event){
	for(@i = 1, @i <= get_value('zperm.regclick.id'), @i++){
		if(!has_value('zperm.regclick.pos.'.@i)){
			continue();
		}
		if(double(get_value('zperm.regclick.pos.'.@i)[0]) == double(@event['location'][0]) && double(get_value('zperm.regclick.pos.'.@i)[1]) == double(@event['location'][1]) && double(get_value('zperm.regclick.pos.'.@i)[2]) == double(@event['location'][2]) && get_value('zperm.regclick.pos.'.@i)[3] == @event['location'][3]){
			_regclickremove(@i);
		}
	}
}

register_command('zutils', array(
	'description': 'Zutils main command.',
	'usage': '/zutils <option>',
	'permission': '*',
));

register_command('z', array(
	'description': 'Zutils main command.',
	'usage': '/z <option>',
	'permission': '*',
));

register_command('zhelp', array(
	'description': 'Displays the Zutils help page.',
	'usage': '/zhelp',
	'permission': '*',
));

register_command('zver', array(
	'description': 'Displays the Zutils version.',
	'usage': '/zver',
));

register_command('zinfo', array(
	'description': 'Displays information about Zutils.',
	'usage': '/zinfo',
));

register_command('feed', array(
	'description': 'Feeds a player.',
	'usage': '/feed <player>',
));

register_command('enchantability', array(
	'description': 'Displays a list of all possible enchantments for an item.',
	'usage': '/enchantability <item>',
));

register_command('ea', array(
	'description': 'Displays a list of all possible enchantments for an item.',
	'usage': '/ea <item>',
));

register_command('start', array(
	'description': 'Teleports the player to a random location.',
	'usage': '/start',
));

register_command('cmsg', array(
	'description': 'Displays more information about custom messages.',
	'usage': '/cmsg <help/l|list/l>',
));

register_command('tmsg', array(
	'description': 'Toggles messaging with another player.',
	'usage': '/tmsg <player/off/help>',
));

set_tabcompleter('feed', closure(@alias, @player, @args, @info) {
    @players = array();
    foreach(@player in all_players()) {
        if(string_position(@player, @args[-1]) == 0) {
            @players[] = @player;
        }
    }
    return(@players);

});

set_tabcompleter('tmsg', closure(@alias, @player, @args, @info) {
    @players = array();
    foreach(@player in all_players()) {
        if(string_position(@player, @args[-1]) == 0) {
            @players[] = @player;
        }
    }
    return(@players);

});

set_tabcompleter('z', closure(@alias, @player, @args, @info) {
    @players = array();
    foreach(@player in all_players()) {
        if(string_position(@player, @args[-1]) == 0) {
            @players[] = @player;
        }
    }
    return(@players);

});

set_tabcompleter('zutils', closure(@alias, @player, @args, @info) {
    @players = array();
    foreach(@player in all_players()) {
        if(string_position(@player, @args[-1]) == 0) {
            @players[] = @player;
        }
    }
    return(@players);

});
