//ZpsiScript Procedures/Functions.  See 'aliases.msa' for usage of such procedures.
//Repository: https://gitlab.com/zpsi/zutils/tree/master/

@prefix = '&8[LOTC] &r';

proc _cmsg (@player, @num){
	@prefix = '&8[LOTC] &r';
	if(!has_permission(@player, 'z.name') && !has_permission(@player, 'z.cmsg')){
		_error(1);
	}
	if(get_value('zutils.cmsg_allowed.'.to_lower(@player)) == false || get_value('zutils.cmsg_allowed.all') == false){
		_error(1);
	}
	@cmd = split(' ', get_cmd())[0];
	if(@num != ''){
		if(to_lower(@arguments[1]) == 'help' || to_lower(@arguments[1]) == 'h'){
			call_alias('/cmsg help');
			die();
		}
		if(to_lower(@arguments[1]) == 'info' || to_lower(@arguments[1]) == 'i'){
			call_alias('/cmsg info');
			die();
		}
		@nArgs = split(' ', @num)
		if(to_lower(@nArgs[0]) == 'list' || to_lower(@nArgs[0]) == 'l'){
			@pages = integer((array_size(@arguments) - 3)/8) + 1;
			if(array_size(@nArgs) > 1){
				if(is_numeric(@nArgs[1])){
					if(@nArgs[1] <= @pages && @nArgs[1] >= 1){
						@page = @nArgs[1];
						@i = 1 + 8*(@page - 1);
					}
					else{
						@page = @pages;
						@i = 1 + 8*(@page - 1);
					}
				}
			}
			else{
				@i = 1;
				@page = 1;
			}			 
			@concatedList = '&9List of possible messages (Page '.@page.'/'.@pages.'):\n'.
			'&8&m-----------------------------------------------------\n';

			for(@i, @i <= @page*6 && @i <= array_size(@arguments) - 2, @i++){
				if(@i < 10){
					@num = '  '.@i;
				}
				else{
					@num = ''.@i;
				}
				@concatedList = concat(@concatedList, '&8', @num, '.\t&7', @arguments[@i+1]);
				if(@i < @page*6 && @i < array_size(@arguments) - 2){
					@concatedList = concat(@concatedList, '\n');
				}
				else{
					@concatedList = concat(@concatedList, '\n&8&m-----------------------------------------------------');
					if(@pages != 1){
						if(@page < @pages){
							@concatedList = concat(@concatedList, '\n&9Do '.@cmd.' list '.(@page + 1).' to get to the next page.');
						}
						else{
							@concatedList = concat(@concatedList, '\n&9Do '.@cmd.' list '.(@page - 1).' to get to the previous page.');
						}
					}				
				}
			}
			tmsg(@player, colorize(@concatedList));
			die();
		}
		if(is_numeric(@num)){
			if(@num < array_size(@arguments) - 1 && @num != 0){
				@msg = @arguments[@num + 1];
			}
			else{
				_error(3);
			}
		}
		else{
			_error(2);
		}
	}
	else{
		if(array_size(@arguments) > 3){
			@i = rand(2, array_size(@arguments));
			@msg = @arguments[@i];
		}
		else{
			@msg = @arguments[2];
		}
	}
	
	if(!has_value('zutils.name_limit.'.@player)){
		chatas(@player, colorize(@msg));
	}
	else{
		tmsg(@player, colorize(@prefix.'&cPlease wait at least 8 seconds between uses.'));
	}
	set_timeout(8000, closure(){
		clear_value('zutils.name_limit.'.@player);
	})
	store_value('zutils.name_limit.'.@player, 1);
}

proc _wait(@seconds, @run){

	@prefix = '&8[LOTC] &r';
	if(has_permission(player(), 'z.wait.override')){
		msg(colorize(@prefix.'&bWait time overridden.'))
		@seconds = 0;
	}
	if(@seconds > 0){

		if(has_value('zutils.wait.taskid.'.to_lower(player()))){
			_waitoff();
		}
		store_value('zutils.wait.canceled.'.to_lower(player()), false);

		store_value('zutils.wait.e.move.'.to_lower(player()),
			bind(player_move, null, array(threshhold: 2, player: player()), @event){
				@prefix = '&8[LOTC] &r';
				msg(colorize(@prefix.'&cCanceled, please try again.'));
				_waitoff();
			}
		);

		store_value('zutils.wait.e.hurt.'.to_lower(player()),
			bind(entity_damage, null, array(id: pinfo(player(), 13)), @event){
				@prefix = '&8[LOTC] &r';
				msg(colorize(@prefix.'&cCanceled, please try again.'));
				_waitoff();
			}
		);

		store_value('zutils.wait.e.quit.'.to_lower(player()),
			bind(player_quit, null, array(player: player()), @event){
				@prefix = '&8[LOTC] &r';
				msg(colorize(@prefix.'&cCanceled, please try again.'));
				_waitoff();
			}
		);

		store_value('zutils.wait.e.death.'.to_lower(player()),
			bind(player_death, null, array(player: player()), @event){
				@prefix = '&8[LOTC] &r';
				msg(colorize(@prefix.'&cCanceled, please try again.'));
				_waitoff();
			}
		);

		msg(colorize(@prefix.'&bPlease don\'t move for '.@seconds.' seconds.'));
		store_value('zutils.wait.taskid.'.to_lower(player()),
			set_timeout(@seconds * 1000, closure(){

				if(!get_value('zutils.wait.canceled.'.to_lower(player()))){
					execute(@run);
				}
				_waitoff();
			})
		)
	}
	else{
		execute(@run);
	}
}

proc _waitoff(){
	clear_task(get_value('zutils.wait.taskid.'.to_lower(player())));
	unbind(get_value('zutils.wait.e.move.'.to_lower(player())));
	unbind(get_value('zutils.wait.e.hurt.'.to_lower(player())));
	unbind(get_value('zutils.wait.e.quit.'.to_lower(player())));
	unbind(get_value('zutils.wait.e.teleport.'.to_lower(player())));
	unbind(get_value('zutils.wait.e.death.'.to_lower(player())));
	clear_value('zutils.wait.e.move.'.to_lower(player()));
	clear_value('zutils.wait.e.hurt.'.to_lower(player()));
	clear_value('zutils.wait.e.quit.'.to_lower(player()));
	clear_value('zutils.wait.e.teleport.'.to_lower(player()));
	clear_value('zutils.wait.e.death.'.to_lower(player()));
	clear_value('zutils.wait.canceled.'.to_lower(player()));
	clear_value('zutils.wait.'.to_lower(player()));
	clear_value('zutils.wait.taskid.'.to_lower(player()));
}

proc _error (@id){
	@prefix = '&8[LOTC] &r';
	@color = '&c';
	switch(@id){
		case 1: @msg = 'Error: Insufficient permissions.';
		case 2: @msg = 'Error: Incorrect usage.';
		case 3: @msg = 'Error: Could not find target.';
		case 4: @msg = 'Error: Invalid target.';
	}
	die(colorize(@prefix.@color.@msg));
}

proc _dbclear(@namespace){
    @a = get_values($);
    foreach(@key: @value in @a){
        clear_value(@key);
    }
}

proc _tmsgoff(){
	@prefix = '&8[LOTC] &r';
    clear_value('zutils.tmsg.'.to_lower(player()));
    clear_value('zutils.tmsg.to.'.to_lower(player()));
    unbind(get_value('zutils.tmsg.e.chatid.'.to_lower(player())));
    unbind(get_value('zutils.tmsg.e.quitid.'.to_lower(player())));
    clear_value('zutils.tmsg.e.chatid.'.to_lower(player()));
    clear_value('zutils.tmsg.e.quitid.'.to_lower(player()));
    die(colorize(@prefix.'&cMessaging has been toggled off.'));
}

proc _regclickrun(@run){
	@prefix = '&8[LOTC] &r';
	switch(@run){
		default:
			colorize(@prefix.'Error: Unknown run id.');
			break();
	}
	die();
}

proc _regclickremove(@i){
	@prefix = '&8[LOTC] &r';
    clear_value('zperm.regclick.run.'.@i);
    clear_value('zperm.regclick.pos.'.@i);
    clear_value('zperm.regclick.action.'.@i);
    clear_value('zperm.regclick.buffer.'.@i);
    die(colorize(@prefix.'&cReg-click event removed.'));
}

proc _regclickaskbuffer(){
	@prefix = '&8[LOTC] &r';
	msg(colorize(@prefix.'&bWhat should the buffer be (in seconds)?'));
    bind(player_chat, null, array(player: player()), @echat){
        cancel()
        if(is_numeric(@echat['message'])){
        	store_value('zperm.regclick.buffer.'.get_value('zperm.regclick.id'), @echat['message']);
			unbind();
			_regclickdone();
        }
        else{
        	unbind();
        	_regclickaskbuffer();
        }
        unbind();
	}
}

proc _regclickdone(){
	@prefix = '&8[LOTC] &r';
    _regclickoff();
    die(colorize(@prefix.'&bBlock at X: '.get_value('zperm.regclick.pos.'.get_value('zperm.regclick.id'))[0].', Y: '.get_value('zperm.regclick.pos.'.get_value('zperm.regclick.id'))[1].', Z: '.get_value('zperm.regclick.pos.'.get_value('zperm.regclick.id'))[2].', in the World: '.get_value('zperm.regclick.pos.'.get_value('zperm.regclick.id'))[3].', registered on Action: '.get_value('zperm.regclick.action.'.get_value('zperm.regclick.id')).', and will Run: \"'.get_value('zperm.regclick.run.'.get_value('zperm.regclick.id')).'\", with a buffer of '.get_value('zperm.regclick.buffer.'.get_value('zperm.regclick.id')).' seconds.'));
}

proc _regclickoff(){
	clear_task(get_value('zutils.regclick.taskid.'.player()));
	clear_value('zutils.regclick.taskid.'.to_lower(player()));
	unbind(get_value('zutils.regclick.e.interactid.'.to_lower(player())));
	clear_value('zutils.regclick.e.interactid.'.to_lower(player()));
	unbind(get_value('zutils.regclick.e.commandid.'.to_lower(player())));
	clear_value('zutils.regclick.e.commandid.'.to_lower(player()));
	unbind(get_value('zutils.regclick.e.chatid.'.to_lower(player())));
	clear_value('zutils.regclick.e.chatid.'.to_lower(player()));
	unbind(get_value('zutils.regclick.remove.e.interactid.'.to_lower(player())));
	clear_value('zutils.regclick.remove.e.interactid.'.to_lower(player()));
	clear_value('zutils.regclick.toggled.'.to_lower(player()));
}